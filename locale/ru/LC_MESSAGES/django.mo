��    �      �  �   �      �  z   �  �     �   �  B   U  �   �    )  V   2  �   �  n     Z   �  p   �  Q   V     �     �  
   �     �     �     �            	   +     5     B  "   O     r  -        �     �     �     �     �     �     �     �        %   "     H  
   \     g     p  	   �     �     �  	   �     �     �     �     �     �  
   �     �               '  
   5     @     H     ]     l  n   y  !   �     
          '     ?     W  n   ^     �     �  G   �  g   5  H   �  -   �               '     -  !   >     `     g     n     v     �     �     �     �  	   �     �     �     �     �     �            	        )     D     V     d     q          �  %   �     �     �     �            4   '  P   \  :   �     �     �     �     �          *     2     6     ?     H     W  )   ]     �     �     �     �     �     �  	   �     �     �  
                    1     C  	   S     ]     d     l     z     �  u   �     &   ,   /   &   \      �      �      �      �      �      �      �      �   f   �      /!     7!     P!  
   ]!     h!     }!  F   �!  h   �!  #   2"     V"  f   u"  U   �"     2#  0   G#     x#  '   �#  =   �#     �#     �#     $     $     $     *$  3   -$     a$     j$  	   q$     {$  �  �$  �   >&  �   �&  �   �'  N   X(  �   �(  }  �)  g   +    +  �   �,  �   @-  �   �-  �   g.  1   �.     "/     >/     W/     r/     �/     �/     �/     �/     �/      0  3   0     P0  M   k0  
   �0     �0     �0     �0     �0     1     )1  
   ?1  >   J1  I   �1  !   �1     �1     2  #   2  !   @2     b2     x2     �2     �2     �2     �2     �2     3  %   &3  )   L3  !   v3     �3  !   �3  %   �3     �3     4     "4     74  �   M4  9   �4     05  %   C5  %   i5     �5     �5  �   �5     �6     �6  �   �6  �   [7  �   8  N   �8     �8     �8     �8     
9  8   )9  
   b9  
   m9  
   x9     �9  6   �9  
   �9     �9     �9     �9     :  (   :  &   6:     ]:  C   }:     �:     �:  %   �:  '   ;     ;;     [;  
   y;     �;     �;  
   �;  l   �;  '   2<  *   Z<      �<     �<  (   �<  ?   �<  �   =  |   �=     B>     [>  
   d>     o>  :   �>     �>     �>     �>  $   �>     ?     9?  <   T?     �?  $   �?  A   �?      @     ,@     ;@     N@     f@  :   y@     �@  ,   �@     �@     A     -A     IA     ZA     gA     pA  *   �A  B   �A  �   �A     �B  =   �B  *   &C  
   QC  
   \C  
   gC     rC     �C     �C     �C     �C  �   �C     �D  '   �D     �D     �D  9   �D     -E  �   2E  �   �E  -   [F  7   �F  �   �F  {   vG  "   �G  M   H     cH  J   �H  ,   �H     �H  &   �H  
   &I     1I  
   >I     II  g   PI     �I     �I     �I  
   �I         q   �         s   ^   �              c   1          *   m   d       �          3   �   l      r   n           C   �      �       )   �   V   (   �           a   e       �              +   �          �       �   �                  �   5   �   �       L   U   �           N   2       R      	   �   b       #       �      �           t                         7       ,   6   W   �       �       �   k   ?          I   �      �   h   �   u   J   o      �   �           w              �   E   �       ]      �       y   p   �   {   
   x   9       �   ;               Y   �   >          �       ~      4   }   �   f                0   M   �   �   `   �   \      .   %       <   �   j   =   [   X      P       �   T   D   $   �   �   z       Q   _       �           �   B   �   :   &              F          K   �       �   �   �   -   Z   i          |       �               A   /   O       �       H   "   v   g   �   �   @   �   '   !   �       �   S   G       �   8    
                        Did you mean <a href="%(search_url)s?q=%(suggestion)s">"%(suggestion)s"</a>?
                     
                    Found <strong>%(num_results)s</strong> result.
                 
                    Found <strong>%(num_results)s</strong> results.
                 
                    Found <strong>%(num_results)s</strong> results, showing <strong>%(start)s</strong> to <strong>%(end)s</strong>.
                 
                Page %(page_num)s of %(total_pages)s
             
                This will delete all information about you from the site.  Deleting your profile cannot be
                undone.
             
            Your order has been placed and a confirmation email has been sent - your order number is
            <strong>%(number)s</strong>.
            Thank you for your order, our manager will contact you shortly. The total price of your order is
             
        <div class="page-header action"><h2>Products matching "%(q)s"</h2></div>
     
        If you don't receive an email, please make sure you've entered the address you registered with, and check
        your spam folder.
     
        We've e-mailed you instructions for resetting your password. You should be receiving it shortly.
     
    You're receiving this e-mail because your password has been changed at %(name)s.
     
    You're receiving this email because your email address has been changed to <strong>%(email)s</strong>.
     
You're receiving this email because your password has been changed at %(name)s.
 %(title|safe)s is back in stock 1. Shipping address 2. Preview 3. Confirmation Account Add a new address Add address Add to basket Adding... Address Book Address book An address from your address book? Archiving... Are you sure you want to delete this address? Basket total: Body Build Cancel Change my password Change password Changing... City Confirmation of order %(number)s Confirmation required for stock alert Contact information Contact us Continue Continue shopping Dashboard Date created Date registered Date sent Date submitted Dear %(name)s, Delete Delete profile Deleting... Edit order Edit profile Email History Email address Email history Email sent English Enter a new password Filter results Filtering... Forgotten your password? Enter your e-mail address below, and we'll e-mail instructions for setting a new one. Found <strong>0</strong> results. Free From cheap to expensive From expensive to cheap Get a password reminder Hello, Hello,

We are pleased to confirm your order %(order_number)s has been received and
will be processed shortly. Home I've forgotten my password If it was you who changed the email address, you can ignore this email. If it wasn't you who changed it, please reset your password immediately and correct your email address: If it wasn't you who changed it, please reset your password immediately: Important messages about items in your basket Inbox Instructions Items Items to buy now Kharkiv 2016. All rights reserved Log In Log in Log out Logging in... Login or register Logout Message More My basket Name New post department No emails found No order No orders match your search. Not Available Notifications Num items Oops! We found some errors Or a new address? Order History Order Total: Order history Order number Order total Otherwise, you can ignore this email. Password reset Password reset complete Password reset unsuccessful Phone Pickup from new-post department Please confirm your password to delete your profile. Please enter your new password twice so we can verify you typed it in correctly. Please go to the following page and choose a new password: Preview Price Product Product Alerts Products you recently viewed Profile Qty Quantity Register Registering... Reset Resetting your password at %(site_name)s. Return Return to basket Return to notifications inbox Return to site Russian Save Saving... Self pickup Send reset email Sending... Ship to this address Shipping address Shipping address: Shipping method Shipping: Status Subject Submitting... Thank you for registering. Thanks for using our site! The password reset link was invalid, possibly because it has already been used.  Please request a new password reset. The team There are no addresses in your address book. There are no notifications to display. Total Total inc tax Totals: Update Updating... Version: View Warning We are pleased to confirm your order %(order_number)s has been received and
will be processed shortly. Welcome Where should we ship to? Who are you? Wish Lists With selected items: Yes! You can view the status of this order at the below URL:
%(status_url)s You can view the status of this order by clicking <a href="%(status_url)s" title="order status">here</a> You could be missing out on offers! You haven't placed any orders. You're receiving this e-mail because you requested a password reset for your user account at %(name)s. You're receiving this email because your email address has been changed to %(email)s. Your basket is empty Your email address has changed at %(site_name)s. Your order contains: Your password changed at %(site_name)s. Your password has been set.  You may go ahead and log in now. cancel continue shopping... is empty next nounArchive or please check the error messages below and try again previous price: quantity: verbArchive Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-09 07:46-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
                       Вы имели в виду <a href="%(search_url)s?q=%(suggestion)s">"%(suggestion)s"</a>?
                    
                   Найдено <strong>%(num_results)s</strong> результата. 
                 
                   Найдено <strong>%(num_results)s</strong> результата. 
                 
                   Найдено <strong>%(num_results)s</strong> результата, отображено<strong>%(start)s</strong> из <strong>%(end)s</strong>. 
                 
                %(page_num)s страниц из %(total_pages)s
             
                Все ваши данные будут безвозвратно удалены, востановить из будет невозможно,вы уверены что хотите удалить профиль?
             
            Ваш заказ был размещен и уведомление было отправленно вам на email. Номер вашего заказа:
            <strong>%(number)s</strong>.
            Наш менеджер свяжется с вами в близжайше время. Сумма вашей покупки составляет 
             
        <div class="page-header action"><h2>Найдено по запросу "%(q)s"</h2></div>
     
        Если вы не получили email, пожалуйста проверьте  правильность вводаemail адреса который вы использовали при регистрации, и проверьте
        вкладку спам
      
        Мы отправили Вам email с инсрукциями для востановления пароля. Вы скоро его получите.
     
    Вы получили это уведомление так как ваш пароль на сайте %(name)s. был изменен 
      
   Выполучили это сообщение так как вай еmail адрес был измененна <strong>%(email)s</strong>.
     
Вы получили это уведомление так как ваш пароль на сайте %(name)s. был изменен 
 %(title|safe)s появился в наличии Адрес доставки Предпросмотр Подтверждение Профиль Добавить данные Добавить адрес B корзину Довавление... Адресная книга Адресная книга Ранее использованный адрес? Архивирование Вы уверенны что хотите удалить эти данные? Итого Тело Построить Отмена Изменить пароль Изменить пароль Изменение... Город Подтверждение заказа номер %(number)s  Требуется уточнение о наличии на складе Контактные данные Контакты: Продолжить Продолжить покупки Панель управления Дата заказа Дата регистрации Дата отправки Дата заказа Уважаемый %(name)s, Удалить Удалить профиль Удаление... Редактировать заказ Редактировать профиль История сообщений Email адрес История сообщений Отправить сообщение Английский Введите пароль Результаты Обработка... Забыли пароль? Введите ваш email адрес ниже, и мы пришлем вам инструкции по его восстановлению. Найдено <strong>0</strong> результатов. Бесплатно От дешевых к дорогим От дорогих к дешевым Забыли пароль? Здравствуйте, Мы рады сообщить что ваш заказ был принят. Номер вашего заказа %(order_number)s. Наш менеджер свяжется с вами в близжайшее время Главная Забыли пароль? Если это вы изменили email адрес, вы можете не обращать внимания на это сообщение. Если адрес сменили не вы, пожалуйста сразу-же измените ваш пароль, и исправте ваш email адрес. Если это не вы изменили пароль, пожалуйста измените ваш пароль немедленно: Важные сообщения о товарах в вашей корзине Входящие Пожелания Товары Товары к покупке Харьков 2016. Все права защишены. Войти Войти Выйти Вход... Войти или зарегистрироваться Выйти Сообщение Подробнее Корзина Имя Отделение новой почты Сообщений не найдено Не отсортировано По вашему запросу ничего не найденно Недоступно Уведомления Колличество товаров Упсс! Мы нашли ошибку! Или новые данные? История заказов Итого История заказов Номер заказа Итого В противном случае, не обращайте внимание на это сообщение. Востановление пароля Смена пароля выполнена Пароль не изменен Телефон Доставка новой почтой Пожалуйста подтвержите ваш пароль Пожалуйста введите ваш пароль дважды, чтоб мы могли удостовериться что вы ввели правильно. Пожалуйста перейдите на следующую страницу и выберите новый пароль Предпросмотр Цена Товар Оповещения Недавно просмотренные продукты Профиль Колличество Количество Зарегистрироваться Регистрация... Перезагрузить Сбросить пароль на сайте %(site_name)s. Вернуться Вернуться в корзину Вернуться ко входящим уведомлениям Вернуться на сайт Русский Сохранить сохранение... Самовывоз Отправить email для востановления Отправка... Использовать эти данные Адрес доставки Адрес доставки Метод доставки Доставка Статус Тема Отправка формы... Спасибо за регистрацию Спасибо за пользование нашим сайтом Ссылка востановления пароля не сработала, возможно она одна жды уже была использованна. Пожалуйста попробуйте еще раз Комманда В вашей адресной книге нет данных Уведомлений не найдено Итого Итого Итого Обновить Обновление... Версия Просмотреть Внимание Ваш заказ был создан. Номер вашего заказа %(order_number)s. Наш менеджер свяжется с вами в близжайшее время Добро пожаловать Куда доставить заказ? Кто вы? Лист желаний C помощью выбранных эллементов: Да Вы можете просмотреть статус вашего заказа перейдя по ссылке ниже
%(status_url)s Вы можете изменить статус вашего заказа кликнув по ссылке <a href="%(status_url)s	itle="order status">here</a> Не упустите предложение! Мы не сделали ни одного заказа Вы получили это уведомление так как вы захотели изменить пароль для вашего аккаунта на сайте %(name)s. Вы получили это сообщение так как ваш email адрес был изменен на %(email)s. Ваша корзина пуста  На сайте %(site_name)s ваш email адрес был изменен. Заказ собержит: Ваш парьль на сайте %(site_name)s. был изменен.  Ваш пароль был сохранен. Нет продолжить покупки... пуста Вперед Архив или пожалуйста прочтите сообщение ниже и повторите попытку. Предыдущая Цена Колличество Архив 