from oscar.apps.promotions import views as oscar_views
from shop.catalogue.models import Product
from shop.general_info.models import HomeImages


class HomeView(oscar_views.HomeView):
    template_name = 'promotions/home.html'
    context_object_name = 'product_list'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['product_list'] = Product.objects.filter(is_important=True)
        context['img_slider'] = HomeImages.objects.all()
        return context
