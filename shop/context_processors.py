from datetime import datetime
from django.contrib.flatpages.models import FlatPage
from django.core.context_processors import request

from shop.general_info.forms import CallbackForm
from shop.general_info.models import ContactInfo


def flatpages(request):
    pages = FlatPage.objects.all()
    return {"pages": pages}


def general(request):
    info = ContactInfo.objects.first()
    return {"info": info}


def test(request):
    return {'form': CallbackForm}
