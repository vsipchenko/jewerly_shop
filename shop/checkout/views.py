from oscar.apps.checkout import views as oscar_views
from django.shortcuts import redirect
from dal import autocomplete

from shop.new_post.models import City, Department


class ShippingMethodView(oscar_views.ShippingMethodView):
    def get_success_response(self):
        return redirect('checkout:preview')


class ShippingAddressView(oscar_views.ShippingAddressView):

    def get_available_addresses(self):
        # Include only addresses where the country is flagged as valid for
        # shipping. Also, use ordering to ensure the default address comes
        # first.
        return self.request.user.addresses.all().order_by('-is_default_for_shipping')


class CityAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = City.objects.all()

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class DepartmentAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs = Department.objects.all()

        new_post_city = self.forwarded.get('new_post_city', None)
        if new_post_city:
            qs = qs.filter(city=new_post_city)

        if self.q:
            qs = qs.filter(description__icontains=self.q)

        return qs

