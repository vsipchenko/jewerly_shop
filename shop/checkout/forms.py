
from django.core.urlresolvers import reverse_lazy
from oscar.apps.address.forms import AbstractAddressForm
from oscar.apps.checkout import forms as oscar_forms
from django.utils.translation import ugettext_lazy as _

from shop.new_post.models import City
from shop.order.models import ShippingAddress
from dal import autocomplete
from django import forms


class ShippingAddressForm(AbstractAddressForm):
    new_post_city = forms.ModelChoiceField(
        label=_('City'),
        required=False,
        queryset=City.objects.all(),
        widget=autocomplete.ModelSelect2(url=reverse_lazy('checkout:city-autocomplete'))
    )

    def __init__(self, *args, **kwargs):
        super(ShippingAddressForm, self).__init__(*args, **kwargs)
        self.fields['phone_number'].required = True

    def clean(self):
        cleaned_data = super(ShippingAddressForm, self).clean()
        shipping_method = cleaned_data.get('shipping_method')
        new_post_department = cleaned_data.get("new_post_department")

        if shipping_method == ShippingAddress.SHIPPING_METHOD_NEW_POST:
            if not new_post_department:
                self.add_error('new_post_department', 'This field is required.')
        return cleaned_data

    def adjust_country_field(self):
        pass

    class Meta(oscar_forms.ShippingAddressForm.Meta):
        model = ShippingAddress
        fields = [
            'first_name', 'last_name', 'phone_number', 'shipping_method', 'new_post_city',
            'new_post_department', 'notes',
        ]
        widgets = {
            'new_post_department': autocomplete.ModelSelect2(url=reverse_lazy('checkout:department-autocomplete'), forward=['new_post_city'])
        }


from django import forms


