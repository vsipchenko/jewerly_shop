$(function () {
    function toggleButton() {
        if (arraysEqual(initIdList, getIdList())) {
            $('#save_image_order').hide()
        } else {
            $('#save_image_order').show()
        }
    }

    function getIdList() {
        var res = [];
        var list_elem = $('td.id');
        $.each(list_elem, function (index, value) {
            res.push($(value).text());
        });
        return res
    }

    var initIdList = getIdList();

    function arraysEqual(a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;

        for (var i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }


    $('tbody').sortable({
        update: toggleButton
    });

    function saveImageOrder() {
        var data = {
            ids: getIdList(),
            action: "order"
        };
        var url = $(this).attr('data-action');
        $.ajax({
            method: "POST",
            url: url,
            data: data,
            traditional: true,
            dataType: "application/json",
            statusCode: {
                200: function () {
                    initIdList = getIdList();
                    toggleButton();
                }
            }
        })
    }


    $('#save_image_order').on('click', saveImageOrder)


});