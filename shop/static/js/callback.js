$(function () {
    $('#call').on('click', function () {
        $("#modal-form").load("/callback");
    });

    function getResult() {
        var url = $('#callback-form').attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: $('#callback-form').serialize(),
            dataType: "json",
            statusCode: {
                201: function (data) {
                    $('.modal-body').empty();
                    $('.modal-footer').empty();
                    $('.modal-body').append($('#callback-form').attr('data-success-title'));
                    console.log(data)
                },
                400: function (data) {
                    console.log(data.responseJSON.errors.phone);
                    $('.form-group.Phone').addClass('has-error');
                    $('.form-group.Phone div.col-sm-9').append("<span class='error-block'>" + data.responseJSON.errors.phone + "</span>")
                }
            }
        })
    }
    $('body').on('click', '#callback-form-button', function (event) {
        event.preventDefault();
        $('.form-group.Phone div.col-sm-9 span.error-block').remove();
        getResult()
    });

});
