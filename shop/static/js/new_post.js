$(function() {

    function func() {
        if($('#id_shipping_method').val() == 'new_post') {
            $('#id_new_post_address, #id_new_post_city, #id_new_post_department').parent().parent().show();
        } else {
            $('#id_new_post_address, #id_new_post_city, #id_new_post_department').parent().parent().hide();
        }
    }

    function dep_clear(){
        $($('.select2-hidden-accessible')[1]).val(null).trigger('change')
    }

    function dep_hide(){
        if($($('.select2-hidden-accessible')[0]).val() == null){
            $($('.select2-container')[1]).addClass('non-pointer');
            dep_clear();
        } else {
            $($('.select2-container')[1]).removeClass('non-pointer');
        }
    }

    func();
    $('#id_shipping_method').on('change', func);

    $('#id_new_post_city').on('select2:select', dep_clear);

    dep_hide();
    $('#id_new_post_city').on('change',dep_hide);
});