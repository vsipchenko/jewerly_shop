# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_auto_20160727_0815'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shippingaddress',
            name='new_post_department',
            field=models.ForeignKey(verbose_name='New post department', blank=True, to='new_post.Department', null=True),
        ),
        migrations.AlterField(
            model_name='shippingaddress',
            name='shipping_method',
            field=models.CharField(default=b'self', max_length=10, verbose_name='Shipping method', choices=[(b'self', 'Self pickup'), (b'new_post', 'Pickup from new-post department')]),
        ),
    ]
