# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_shippingaddress_shipping_method'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shippingaddress',
            name='new_post_address',
        ),
    ]
