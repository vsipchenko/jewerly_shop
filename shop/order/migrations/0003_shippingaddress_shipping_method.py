# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20160704_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='shipping_method',
            field=models.CharField(default=b'self', max_length=10, choices=[(b'self', 'Self pickup'), (b'new_post', 'Pickup from new-post department')]),
        ),
    ]
