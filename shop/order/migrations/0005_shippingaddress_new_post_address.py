# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_post', '0004_department'),
        ('order', '0004_remove_shippingaddress_new_post_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='shippingaddress',
            name='new_post_address',
            field=models.ForeignKey(blank=True, to='new_post.Department', null=True),
        ),
    ]
