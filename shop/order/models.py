from oscar.apps.address.abstract_models import AbstractShippingAddress
from oscar.apps.address.models import Country

from shop.address.models import AbstractShippingMethod


class ShippingAddress(AbstractShippingAddress, AbstractShippingMethod):

    def save(self, *args, **kwargs):
        self.country = Country.objects.first()
        super(AbstractShippingAddress, self).save(*args, **kwargs)


from oscar.apps.order.models import *  # noqa
