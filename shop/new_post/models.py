from django.db import models


class City(models.Model):
    name = models.CharField(max_length=100)
    city_ref = models.CharField(max_length=100, unique=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class Department(models.Model):
    city = models.ForeignKey('City')
    description = models.CharField(max_length=100)
    number = models.IntegerField()
    site_key = models.IntegerField(unique=True, null=True)

    def __unicode__(self):
        return self.description
