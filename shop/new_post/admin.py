from shop.new_post.models import City, Department
from django.contrib import admin

admin.site.register(City)
admin.site.register(Department)