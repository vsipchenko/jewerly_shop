# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_post', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='site_key',
            field=models.IntegerField(unique=True, null=True),
        ),
    ]
