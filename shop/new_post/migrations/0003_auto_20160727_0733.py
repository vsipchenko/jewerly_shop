# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_post', '0002_auto_20160726_0557'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='department',
            name='city',
        ),
        migrations.DeleteModel(
            name='Department',
        ),
    ]
