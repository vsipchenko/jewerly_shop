# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_post', '0003_auto_20160727_0733'),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
                ('number', models.IntegerField()),
                ('site_key', models.IntegerField(unique=True, null=True)),
                ('city', models.ForeignKey(to='new_post.City')),
            ],
            options={
                'default_related_name': 'Department',
            },
        ),
    ]
