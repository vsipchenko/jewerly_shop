import requests

from django.core.management.base import BaseCommand
from django.conf import settings

from shop.new_post.models import City, Department

class Command(BaseCommand):
    help = 'Adding cities and description of new post department'

    def handle(self, *args, **options):
        headers = {
            'Content-Type': 'application/json',
            }
        data = {
            "modelName": "AddressGeneral",
            "calledMethod": "getWarehouses",
            "apiKey": settings.API_KEY
        }
        response = requests.post('http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses', json=data, headers=headers)

        if response.status_code != 200:
            self.stdout.write('Unexpected response from server - {}. Status - {}'.format(response.text, response.status_code))
            return

        res = []
        for item in response.json()['data']:
            city, created = City.objects.get_or_create(city_ref=item['CityRef'])
            if created:
                city.name = item["CityDescriptionRu"]
                city.save()
            dep = Department.objects.filter(site_key=item['SiteKey']).first()
            if dep:
                dep.city = city
                dep.description = item['DescriptionRu']
                dep.number = int(item['Number'])
                dep.site_key = int(item['SiteKey'])
                dep.save()
            else:
                res.append(
                    Department(city=city, description=item['DescriptionRu'], number=int(item['Number']))
                )
            self.stdout.write(item['DescriptionRu'])
        Department.objects.bulk_create(res)
        print 'All addresses of new post department were successfully added'
