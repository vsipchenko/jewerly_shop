from django import forms
from django.forms.models import ModelForm

from shop.general_info.models import Callback


class CallbackForm(ModelForm):
    class Meta:
        model = Callback
        fields = ['name', 'phone']

    def __init__(self, *args, **kwargs):
        super(CallbackForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs['class'] = 'form-control'

