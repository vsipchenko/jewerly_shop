# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def create_communication_event_types(apps, schema_editor):
    CommunicationEventType = apps.get_model('customer', 'CommunicationEventType')
    items = [
        CommunicationEventType(code='ORDER_PLACED',
                               email_subject_template='Confirmation of order',
                               email_body_template='''
                                We are pleased to confirm your order {{ order_number }} has been received and
                                will be processed shortly.

                                Your order contains:

                                {% for line in order.lines.all %} * {{ line.title }} - quantity:{{ line.quantity }} - {% trans 'price:' %} {{ line.line_price_incl_tax|currency:order.currency }}
                                {% endfor %}
                                Basket total: {{ order.basket_total_incl_tax|currency:order.currency }}
                                Shipping:{{ order.shipping_incl_tax|currency:order.currency }}
                                Order Total: {{ order.total_incl_tax|currency:order.currency }}

                                Shipping address:

                                {% for field in order.shipping_address.active_address_fields %}  {{ field }}
                                {% endfor %}

                                {% if status_url %}
                                You can view the status of this order at the below URL:
                                {{ status_url }}
                                {% endif %}

                                The team
                                ''',
                               email_body_html_template='''
                                <p>We are pleased to confirm your order {{ order_number }} has been received and
                                will be processed shortly.</p>

                                <p>Your order contains:</p>

                                <ul>
                                    {% for line in order.lines.all %}
                                    <li>{{ line.title }} - quantity: {{ line.quantity }} - price: {{ line.line_price_incl_tax|currency:order.currency }}</li>
                                    {% endfor %}
                                </ul>
                                <p>
                                Basket total: {{ order.basket_total_incl_tax|currency:order.currency }}<br/>
                                Shipping: {{ order.shipping_incl_tax|currency:order.currency }}<br/>
                                Order Total: {{ order.total_incl_tax|currency:order.currency }}
                                </p>

                                <p>Shipping address:</p>
                                <p>{% for field in order.shipping_address.active_address_fields %}  {{ field }}<br/>
                                {% endfor %}</p>

                                {% if status_url %}
                                <p>You can view the status of this order by clicking <a href="{{ status_url }}" title="order status">here</a></p>
                                {% endif %}
                               ''',
                               ),
        CommunicationEventType(code='REGISTRATION',
                               email_subject_template='Thank you for registering.',
                               email_body_template='''
                               <p>Thank you for registering.</p>
                               ''',
                               email_body_html_template='''
                               Thank you for registering.
                               ''',
                               ),
        CommunicationEventType(code='PASSWORD_RESET',
                               email_subject_template='Your password was reset',
                               email_body_template='''
                               You're receiving this e-mail because you
                               requested a password reset for your user account at {{ name }}.

                               Please go to the following page and choose a new password:{{ reset_url }}
                               ''',
                               email_body_html_template='''
                               <p>You're receiving this e-mail because you
                               requested a password reset for your user account at {{ name }}.</p>

                               <p>Please go to the following page and choose a new password:</p>
                               {{ reset_url }}
                               ''',
                               ),
        CommunicationEventType(code='EMAIL_CHANGED',
                               email_subject_template='Tour email was changed',
                               email_body_template='''
                               You're receiving this e-mail because you
                               requested a password reset for your user account at {{ name }}.

                               Please go to the following page and choose a new password:{{ reset_url }}
                               ''',
                               email_body_html_template='''
                               <p>You're receiving this e-mail because you
                               email- address has been changed to <strong>{{ email }}</strong>.</p>

                               <p>If it wasn't you who changed it,
                               please reset your password immediately and correct your email address:
                               http://{{ site.domain }}{{ reset_url }}</p>

                               <p>If it was you who changed the email address, you can ignore this email.</p>
                               ''',
                               ),
        CommunicationEventType(code='PASSWORD_CHANGED',
                               email_subject_template='Tour password was changed',
                               email_body_template='''
                               You're receiving this e-mail because you
                               have changed password at {{ name }}.

                               If it wasn't you who changed it,
                               please reset your password immediately:
                               http://{{ site.domain }}{{ reset_url }}

                               If it was you who changed the password, you can ignore this email.
                               ''',
                               email_body_html_template='''
                               <p>You're receiving this e-mail because you
                               email- address has been changed to <strong>{{ email }}</strong>.</p>

                               <p>If it wasn't you who changed it,
                               please reset your password immediately:
                               http://{{ site.domain }}{{ reset_url }}</p>

                               <p>If it was you who changed the password, you can ignore this email.</p>>
                               ''',
                               )
    ]
    CommunicationEventType.objects.bulk_create(items)


def delete_communication_event_types(apps, schema_editor):
    CommunicationEventType = apps.get_model('customer', 'CommunicationEventType')
    code_list = [
        'ORDER_PLACED',
        'REGISTRATION',
        'PASSWORD_RESET',
        'EMAIL_CHANGED',
        'PASSWORD_CHANGED'
    ]
    CommunicationEventType.objects.filter(code__in=code_list).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('general_info', '0006_callback'),
    ]

    operations = [
        migrations.RunPython(create_communication_event_types, delete_communication_event_types),
    ]
