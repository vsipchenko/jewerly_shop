# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general_info', '0003_auto_20160728_0843'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='homeimages',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='homeimages',
            name='order',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
