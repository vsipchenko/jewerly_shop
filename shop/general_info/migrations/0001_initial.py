# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_first', models.CharField(max_length=10, null=True, blank=True)),
                ('phone_second', models.CharField(max_length=10, null=True, blank=True)),
                ('address', models.CharField(max_length=100, null=True, blank=True)),
                ('other', models.CharField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='HomeImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveIntegerField(null=True)),
                ('image', models.ImageField(height_field=441.22, width_field=1286, null=True, upload_to=b'', blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
