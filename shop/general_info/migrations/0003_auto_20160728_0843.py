# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general_info', '0002_auto_20160728_0840'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='homeimages',
            options={},
        ),
        migrations.RemoveField(
            model_name='homeimages',
            name='order',
        ),
    ]
