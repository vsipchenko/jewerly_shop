# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general_info', '0007_custom_create_communicationeventtypes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homeimages',
            name='image',
            field=models.ImageField(default=12, upload_to=b'homepage_slider/'),
            preserve_default=False,
        ),
    ]
