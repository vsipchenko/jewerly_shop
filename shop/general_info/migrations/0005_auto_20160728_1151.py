# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general_info', '0004_auto_20160728_0844'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactinfo',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='homeimages',
            name='image',
            field=models.ImageField(null=True, upload_to=b'homepage_slider/', blank=True),
        ),
    ]
