from shop.general_info.models import ContactInfo, HomeImages, Callback

from django.contrib import admin


class HomeImagesAdmin(admin.ModelAdmin):
    list_display = ('image', 'order')


class ContactInfoAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        items = ContactInfo.objects.all()
        if items.count() >= 1:
            return False
        else:
            return True

admin.site.register(ContactInfo, ContactInfoAdmin)
admin.site.register(HomeImages, HomeImagesAdmin)
admin.site.register(Callback)


