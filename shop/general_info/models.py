from django.db import models
from django.db.models.aggregates import Max
from phonenumber_field.modelfields import PhoneNumberField


class ContactInfo(models.Model):
    phone_first = models.CharField(max_length=100, null=True, blank=True)
    phone_second = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    other = models.CharField(max_length=100, null=True, blank=True)


class HomeImages(models.Model):
    order = models.PositiveIntegerField(null=True)
    image = models.ImageField(upload_to='homepage_slider/')

    class Meta:
        ordering = ['order']

    def save(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        max_order = HomeImages.objects.all().aggregate(Max('order'))['order__max']
        self.order = max_order + 1
        super(HomeImages, self).save(*args, **kwargs)


class Callback(models.Model):
    phone = PhoneNumberField()
    name = models.CharField(max_length=20, null=True, blank=True)
    notes = models.CharField(max_length=100, null=True, blank=True)


