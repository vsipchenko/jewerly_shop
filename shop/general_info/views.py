from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.core.mail import send_mail
from oscar.apps.customer.models import CommunicationEventType
from oscar.apps.dashboard.nav import default_access_fn

from shop.general_info.models import ContactInfo

from shop.general_info.forms import CallbackForm
from django.core import mail
from django.conf import settings
from django.views.generic.edit import UpdateView


def callback_view(request):
    form = CallbackForm()
    if request.method == 'GET':
        return render(request, 'elements/callback_form.html', {'callback_form': form})
    if request.method == 'POST':
        form = CallbackForm(request.POST)
        if form.is_valid():
            form.save()
            to_email = ContactInfo.objects.first().email
            phone = form.cleaned_data.get('phone')
            name = form.cleaned_data.get('name')
            with mail.get_connection() as connection:
                mail.EmailMessage('New callback was ordered',
                                  'User {} with phone {} ordered a callback on site {}'.format(name, phone, settings.OSCAR_SHOP_NAME),
                                  settings.OSCAR_FROM_EMAIL,
                                  to=[to_email],
                                  connection=connection).send()

            return JsonResponse({'success': True, 'errors': None}, status=201)
        else:

            return JsonResponse({'success': False, 'errors': form.errors}, status=400)








