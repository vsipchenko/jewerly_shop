from oscar.app import application
from django.conf.urls import url

from shop.general_info import views


urlpatterns = [
    # Your other URLs
    url(r'^callback/$', views.callback_view, name='callback'),
    # url(r'', include(application.urls)),
    ]
