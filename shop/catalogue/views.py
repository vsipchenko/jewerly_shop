from oscar.apps.catalogue import views as oscar_views
from shop.catalogue.forms import SortingForm


class ProductCategoryView(oscar_views.ProductCategoryView):

    def get_page_list(self, paginator):
        curr_page_num = self.request.GET.get('page', '')
        if not curr_page_num.isdigit():
            curr_page_num = 1
        curr_page_num = int(curr_page_num)
        last_page_num = paginator.num_pages
        curr_page = paginator.page(curr_page_num)
        prev_page_num = None
        next_page_num = None
        if curr_page.has_previous():
            prev_page_num = curr_page.previous_page_number()
        if curr_page.has_next():
            next_page_num = curr_page.next_page_number()
        param = self.request.GET.copy()
        if 'page' in param:
            param.pop('page')
        res = []

        if curr_page_num > 1:
            res.append({'url': '?page={}{}'.format(prev_page_num, '&'+param.urlencode()), 'title': 'Prev', 'active': True, 'direction': 'previous'})
            if curr_page_num > 3:
                res.append({'url': '?page=1', 'title': '1', 'active': True},)
                res.append({'url': None, 'title': None, 'active': True,'direction': 'active'})
            if curr_page_num == 3:
                res.append({'url': '?page=1', 'title': '1', 'active': True})
            res.append({'url': '?page={}{}'.format(prev_page_num, '&'+param.urlencode()), 'title': prev_page_num, 'active': True, 'direction': 'previous'})
        else:
            res.append({'url': None, 'title': 'Prev', 'active': False, 'direction': 'previous active'})

        res.append({'url': None, 'title': curr_page_num, 'active': False, 'direction': 'active'},)

        if curr_page_num < last_page_num:
            res.append({'url': '?page={}{}'.format(next_page_num, '&'+param.urlencode()), 'title': next_page_num, 'active': True, 'direction': 'next'})
            if last_page_num - curr_page_num > 2:
                res.append({'url': None, 'title': None, 'active': True,'direction': 'active'})
                res.append({'url': '?page={}{}'.format(last_page_num, '&'+param.urlencode()), 'title': last_page_num, 'active': True})
            if last_page_num - curr_page_num == 2:
                res.append({'url': '?page={}{}'.format(last_page_num, '&'+param.urlencode()), 'title': last_page_num, 'active': True})
            res.append({'url': '?page={}{}'.format(next_page_num, '&'+param.urlencode()), 'title': 'Next', 'active': True,'direction': 'next'})
        else:
            res.append({'url': None, 'title': 'Next', 'active': False,'direction': 'next active',})
        return res

    def get_context_data(self, **kwargs):
        context = super(ProductCategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category
        search_context = self.search_handler.get_search_context_data(
            self.context_object_name)
        context.update(search_context)
        context['sorting_form'] = SortingForm(initial={'order': self.request.GET.get('order')})
        context['page_list'] = self.get_page_list(context['paginator'])
        return context



case_curr_page_is_5 = [
    {'url': '.../?page=4', 'title': 'prev', 'active': True},
    {'url': '.../?page=1', 'title': '1', 'active': True},
    {'url': '.../?page=3', 'title': '...', 'active': True},
    {'url': '.../?page=4', 'title': '4', 'active': True},
    {'url': None, 'title': '5', 'active': False},
    {'url': '.../?page=6', 'title': '6', 'active': True},
    {'url': '.../?page=7', 'title': '...', 'active': True},
    {'url': '.../?page=10', 'title': '10', 'active': True},
    {'url': '.../?page=6', 'title': 'next', 'active': True},
]

case_curr_page_is_1 = [
    {'url': None, 'title': 'prev', 'active': False},
    {'url': None, 'title': '1', 'active': False},
    {'url': '.../?page=2', 'title': '2', 'active': True},
    {'url': '.../?page=3', 'title': '...', 'active': True},
    {'url': '.../?page=2', 'title': 'next', 'active': True},
]

