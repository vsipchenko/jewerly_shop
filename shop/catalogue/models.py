from django.db import models
from oscar.apps.catalogue.abstract_models import AbstractProduct
import django_filters
from django.utils.translation import ugettext_lazy as _


class AbstractProductHomePage(models.Model):
    is_important = models.BooleanField(_('display on homepage'), default=False)

    class Meta:
        abstract = True


class Product(AbstractProduct, AbstractProductHomePage):
    pass


class ProductFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iexact')

    price = django_filters.NumberFilter()
    price__gt = django_filters.NumberFilter(name='price', lookup_expr='gt')
    price__lt = django_filters.NumberFilter(name='price', lookup_expr='lt')

    # class Meta:
    #     model = Product
    #     fields = ['price', 'release_date']

from oscar.apps.catalogue.models import *  # noqa
