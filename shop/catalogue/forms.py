from django import forms
from django.utils.translation import ugettext_lazy as _


class SortingForm(forms.Form):
    STARTS_WITH_CHEAP = "price"
    STARTS_WITH_EXPENSIVE = "-price"
    EMPTY_CHOICE = ''
    ORDER_CHOICES = (
        (EMPTY_CHOICE, _('No order')),
        (STARTS_WITH_CHEAP, _('From cheap to expensive')),
        (STARTS_WITH_EXPENSIVE, _('From expensive to cheap'))
    )
    order = forms.ChoiceField(choices=ORDER_CHOICES, label="", help_text="",)
