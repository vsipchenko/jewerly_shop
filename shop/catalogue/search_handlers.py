from oscar.apps.catalogue.search_handlers import SimpleProductSearchHandler

from shop.catalogue.models import Product


class OrderProductSearchHandler(SimpleProductSearchHandler):
    def get_queryset(self):
        qs = Product.browsable.base_queryset()
        # import pdb; pdb.set_trace()
        if self.categories:
            qs = qs.filter(categories__in=self.categories).distinct()
            # import pdb; pdb.set_trace()
        if 'order' in self.kwargs:
            param = self.kwargs['order']
            if param == 'price':
                qs = qs.order_by('stockrecords__price_excl_tax')
            if param == '-price':
                qs = qs.order_by('-stockrecords__price_excl_tax')
        return qs
    pass