# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0009_product_is_important'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='is_important',
            field=models.BooleanField(default=False, verbose_name='display on homepage'),
        ),
    ]
