from django_tables2.columns.base import Column
from django_tables2.columns.templatecolumn import TemplateColumn
from oscar.apps.dashboard.tables import DashboardTable
import django_tables2 as tables
from shop.general_info.models import HomeImages
from django.utils.html import format_html
from sorl.thumbnail import get_thumbnail


class ImageColumn(tables.Column):
    def render(self, value):
        im = get_thumbnail(value, '291x100', crop='center')
        html = '<a href="{url}" target="_blank"><img src={im_url}></a>'
        return format_html(html, url=value.url, im_url=im.url)


class HomeImagesTable(DashboardTable):
    image = ImageColumn(order_by='order',
                        verbose_name='Images',
                        attrs={'th': {'class': 'col-sm-10'}})
    delete_btn = TemplateColumn(
        '<button type="submit" class="btn btn-danger" name="delete_id" value="{{record.id}}">Delete</button>',
        verbose_name='Actions',
        attrs={'th': {'class': 'col-sm-2'}})

    class Meta(DashboardTable.Meta):
        model = HomeImages
        fields = ('id', 'image', 'delete_btn')
