from oscar.apps.dashboard.catalogue import config


class CatalogueDashboardConfig(config.CatalogueDashboardConfig):
    name = 'shop.dashboard.catalogue'
