import oscar.apps.dashboard.catalogue.forms as oscar_forms


class ProductForm(oscar_forms.ProductForm):
    class Meta(oscar_forms.ProductForm.Meta):
        fields = ['title', 'upc', 'description', 'is_important',
                  'is_discountable', 'structure']


class StockRecordForm(oscar_forms.StockRecordForm):
    class Meta(oscar_forms.StockRecordForm.Meta):
        fields = [
            'partner', 'partner_sku','price_currency',
            'price_excl_tax','num_in_stock', 'low_stock_threshold',
        ]