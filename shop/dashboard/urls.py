from django.conf.urls import url
from shop.dashboard.views import HomeImagesList, HomeImagesCreateView
from shop.dashboard.views import ContactInfoUpdate

urlpatterns = [
    # Your other URLs
    url(r'^dashboard/contact_info/update/$', ContactInfoUpdate.as_view(
        template_name='dashboard/contact_info_update.html'),
        name='contact_info_update'),
    url(r'^dashboard/home_image/$', HomeImagesList.as_view(),
        name='home_images_list'),
    url(r'^dashboard/home_image/create/$', HomeImagesCreateView.as_view(
        template_name='dashboard/home_images_create.html'),
        name='home_images_create'),

]
