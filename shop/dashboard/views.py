from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic.edit import UpdateView
from shop.dashboard.tables import HomeImagesTable
from shop.general_info.models import ContactInfo
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormMixin
from django_tables2.views import SingleTableMixin
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse_lazy
from shop.general_info.models import HomeImages


class HomeImagesList(SingleTableMixin, FormMixin, TemplateView):
    template_name = 'dashboard/home_images_list.html'
    table_pagination = True
    model = HomeImages
    table_class = HomeImagesTable
    context_table_name = 'images'

    def get_queryset(self):
        qs = self.model.objects.all()
        return qs

    def post(self, request, *args, **kwargs):
        if 'delete_id' in request.POST:
            obj = get_object_or_404(HomeImages, id=request.POST['delete_id'])
            obj.delete()
            return HttpResponseRedirect('.')
        if request.POST.get('action') == 'order':
            list_id = request.POST.getlist('ids')
            obj_list = HomeImages.objects.filter(id__in=map(int, list_id))
            order = 1
            for obj in obj_list:
                obj.order = order
                obj.save()
                order += 1
            return JsonResponse({'success': True})


class HomeImagesCreateView(CreateView):
    model = HomeImages
    fields = ['image']
    success_url = reverse_lazy('home_images_list')


class ContactInfoUpdate(UpdateView):
    model = ContactInfo
    fields = '__all__'
    success_url = reverse_lazy('dashboard:index')

    def get_object(self):
        return ContactInfo.objects.get()






