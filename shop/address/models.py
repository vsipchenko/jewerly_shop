from django.db import models
from django.utils.translation import ugettext_lazy as _
from oscar.apps.address.abstract_models import AbstractUserAddress


class AbstractShippingMethod(models.Model):
    SHIPPING_METHOD_SELF = 'self'
    SHIPPING_METHOD_NEW_POST = 'new_post'
    SHIPPING_METHOD_CHOICES = (
        (SHIPPING_METHOD_SELF, _('Self pickup')),
        (SHIPPING_METHOD_NEW_POST, _('Pickup from new-post department'))
    )
    new_post_department = models.ForeignKey('new_post.Department', null=True, blank=True, verbose_name=_('New post department'))
    shipping_method = models.CharField(max_length=10, default=SHIPPING_METHOD_SELF, choices=SHIPPING_METHOD_CHOICES, verbose_name=_('Shipping method'))

    class Meta:
        abstract = True


class UserAddress(AbstractUserAddress, AbstractShippingMethod):
    pass

from oscar.apps.address.models import *  # noqa
