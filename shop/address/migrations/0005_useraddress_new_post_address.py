# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('new_post', '0004_department'),
        ('address', '0004_remove_useraddress_new_post_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraddress',
            name='new_post_address',
            field=models.ForeignKey(blank=True, to='new_post.Department', null=True),
        ),
    ]
