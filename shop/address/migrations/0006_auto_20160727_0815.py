# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0005_useraddress_new_post_address'),
    ]

    operations = [
        migrations.RenameField(
            model_name='useraddress',
            old_name='new_post_address',
            new_name='new_post_department',
        ),
    ]
