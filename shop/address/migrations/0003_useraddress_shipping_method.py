# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0002_useraddress_new_post_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraddress',
            name='shipping_method',
            field=models.CharField(default=b'self', max_length=10, choices=[(b'self', 'Self pickup'), (b'new_post', 'Pickup from new-post department')]),
        ),
    ]
