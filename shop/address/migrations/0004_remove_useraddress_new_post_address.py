# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0003_useraddress_shipping_method'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='useraddress',
            name='new_post_address',
        ),
    ]
