from oscar.apps.address.forms import AbstractAddressForm
from oscar.apps.address import forms as oscar_forms
from shop.address.models import UserAddress
from shop.checkout.forms import ShippingAddressForm


class UserAddressForm(ShippingAddressForm):

    def __init__(self, user, *args, **kwargs):
        super(UserAddressForm, self).__init__(*args, **kwargs)
        self.instance.user = user
