#!/bin/bash
python manage.py collectstatic --noinput
python manage.py migrate --noinput
gunicorn shop.wsgi:application -w 2 -b :8000