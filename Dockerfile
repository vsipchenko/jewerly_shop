FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /shop
ADD . /shop/
WORKDIR /shop
RUN pip install -r requirements/production.txt
