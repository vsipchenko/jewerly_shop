PROJECT_NAME = shop
STATIC_LIBS_DIR = ./$(PROJECT_NAME)/static/libs

default: lint test

test:
	# Run all tests and report coverage
	# Requires coverage
	python manage.py makemigrations --dry-run | grep 'No changes detected' || \
		(echo 'There are changes which require migrations.' && exit 1)
	coverage run manage.py test
	coverage report -m --fail-under 80
	npm test

lint-py:
	# Check for Python formatting issues
	# Requires flake8
	flake8 .

lint-js:
	# Check JS for any problems
	# Requires jshint
	find -name "*.js" -not -path "${STATIC_LIBS_DIR}*" -print0 | xargs -0 jshint

lint: lint-py lint-js

$(STATIC_LIBS_DIR):
	mkdir -p $@

update-static-libs: $(LIBS)

# Generate a random string of desired length
generate-secret: length = 32
generate-secret:
	@strings /dev/urandom | grep -o '[[:alnum:]]' | head -n $(length) | tr -d '\n'; echo

conf/%.pub.ssh:
	# Generate SSH deploy key for a given environment
	ssh-keygen -t rsa -b 4096 -f $*.priv -C "$*@${PROJECT_NAME}"
	@mv $*.priv.pub $@

staging-deploy-key: conf/staging.pub.ssh

production-deploy-key: conf/production.pub.ssh

# Translation helpers
makemessages:
	# Extract English messages from our source code
	python manage.py makemessages --ignore 'conf/*' --ignore 'docs/*' --ignore 'requirements/*' \
		--no-location --no-obsolete -l en

compilemessages:
	# Compile PO files into the MO files that Django will use at runtime
	python manage.py compilemessages

pushmessages:
	# Upload the latest English PO file to Transifex
	tx push -s

pullmessages:
	# Pull the latest translated PO files from Transifex
	tx pull -af

setup:
	virtualenv -p `which python2.7` $(WORKON_HOME)/shop
	$(WORKON_HOME)/shop/bin/pip install -U pip wheel
	$(WORKON_HOME)/shop/bin/pip install -Ur requirements/dev.txt
	$(WORKON_HOME)/shop/bin/pip freeze
	cp shop/settings/local.example.py shop/settings/local.py
	echo "export DJANGO_SETTINGS_MODULE=shop.settings.local" >> $(WORKON_HOME)/shop/bin/activate
	echo "unset DJANGO_SETTINGS_MODULE" >> $(WORKON_HOME)/shop/bin/postdeactivate
	createdb -E UTF-8 shop
	$(WORKON_HOME)/shop/bin/python manage.py migrate

	@echo
	@echo "The shop project is now setup on your machine."
	@echo "Run the following commands to activate the virtual environment and run the"
	@echo "development server:"
	@echo
	@echo "	workon shop"
	@echo "	python manage.py runserver"

deploy-web:
	docker-compose -f docker-compose-prod.yml build web

.PHONY: default test lint lint-py lint-js generate-secret makemessages \
		pushmessages pullmessages compilemessages docs

.PRECIOUS: conf/%.pub.ssh
