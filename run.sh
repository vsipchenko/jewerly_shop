#!/bin/bash
cd shop/
pip install -r requirements/dev.txt
python manage.py collectstatic --noinput
#python manage.py migrate --noinput
python manage.py runserver 0.0.0.0:8000